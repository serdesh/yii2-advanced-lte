<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2advancedlte',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
//            'viewPath' => '@common/mail',
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.mail.ru',
//                'username' => 'your@mail.ru',
//                'password' => 'your_password',
//                'port' => '465',
//                'encryption' => 'ssl',
//            ],
        ],
        'modules' => [
            'gridview' =>  [
                'class' => '\kartik\grid\Module'
            ]
        ],
    ],
];
